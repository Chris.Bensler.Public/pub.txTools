@ECHO OFF

ECHO Resizing TGA Images...
FOR /R ..\textures %%I IN (*) DO (
  IF /I "%%~xI" EQU ".TGA" resize_image "%%~dpI%%~nxI" 50%%
)
ECHO Done
