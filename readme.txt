---------------------------------------------------------------------------------
-- TXTools Batch Utilities
-- Version 1.1.0
-- Created by Chris Bensler

---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
Zip Contents:
---------------------------------------------------------------------------------
  DXT\resize_image.exe
  DXT\nvdxt.exe
  DXT\readdxt.exe
  DXT\reduceDDS.bat
  DXT\DDStoTGA.bat
  DXT\resizeTGA.bat
  DXT\TGAtoDDS.bat
  DXT\deleteTGA.bat
  DXT\Readme.txt

nvdxt.exe and readdxt.exe are command line tools for converting/writing and reading dxt/dds files and are
part of NVIDIA's DXT Tools.
The files came from an older, much smaller version 5.11 of DDS DXT Tools, that I downloaded from TheLys:
  http://www.thelys.org/toolsmisc.php

---------------------------------------------------------------------------------
Description:
---------------------------------------------------------------------------------
  TXTools is a small set of batch files I made to automate the process of reducing the size/quality of textures
  in order to improve FPS rates.

  For efficiency when one wants to reduce a set of textures multiple times in a row, I have included batch
  files for each step of the process.

  As an example, I was able to reduce LowFatGraphics to 1/3 the size with satisfactory results and
  an avg FPS increase of about 4 to 6.

---------------------------------------------------------------------------------
Usage:
---------------------------------------------------------------------------------
  Copy the TXTools folder to the \data_files\TXTools directory and run the batch files from there.
  All the batch files will operate on all the appropriate files in the data_files\textures folder including subfolders.

  reduceDDS.bat
    :resizes all the DDS files in the textures folder in place, overwriting the originals.
     This calls on all the other batch files for a one click (ok, a doublelclick :P) process.

  Using the other batch files you can make your work more efficient when you want to perform multiple reductions on the textures
  since the intermediate TGA files can be retained.
  
  DDStoTGA.bat
    :calls readdxt.exe to convert all the DDS files in the textures folder to tga files, keeping the orignals.
  resizeTGA.bat
    :calls resize_image.exe to resize all the TGA files in the textures folder to 50% the original size, overwriting the originals.
  TGAtoDDS.bat
    :calls nvdxt.exe to convert all the TGA files in the textures folder to DDS files, keeping the originals.
  deleteTGA.bat
    :deletes all the TGA files in the textures folder


  I do not recommend working on original files since some changes cannot be undone, backup your files first.
  This will save you alot of trouble and will allow you to abandon your efforts if the reductions produce
  unsatisfactory results. I also suggest backing up your processed textures before trying to process them again,
  so you can revert if you don't like the results.

  Another suggestion is to be selective of which textures you will reduce for optimal quality vs optimization.
  Some textures are too small/detailed to be reduced, others can be reduced several times without any noticable
  or significant difference in game gfx quality. Some textures will also obviously impact the FPS alot more than others
  depending on how they are used, such as landscape textures.

---------------------------------------------------------------------------------
ChangeLog:
---------------------------------------------------------------------------------

Version 1.1.0:
  Created custom command line util to resize TGA's (resize_image.exe).
  Replaced mogrify.exe with resize_image.exe.
  Minor corrections to status msg's in batch files.

Version 1.0.3:
  Corrected order of batch calls in reduceDDS.bat to fix a problem with converted TGA's being deleted prematurely.

Version 1.0.2:
  Fixed it to delete pre-existing TGA's after being converted to DDS.
  Not a major issue, v1.0.1 should still work since the TGA's get overwritten anyways.
  
Version 1.0.1:
  Fixed an issue with pre-existing tga files in the texture folders.
  All pre-existing TGA's will now be converted to DDS before processing.