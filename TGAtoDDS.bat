@ECHO OFF

ECHO Converting TGA Textures to DDS...
PUSHD ..\textures
..\txtools\nvdxt -file *.tga -deep -outsamedir
POPD
ECHO Done