@ECHO OFF

ECHO Deleting TGA Images...
FOR /R ..\textures %%I IN (*) DO (
  IF /I "%%~xI" EQU ".TGA" DEL "%%~dpI%%~nxI"
)
ECHO Done
